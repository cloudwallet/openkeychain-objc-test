//
//  main.m
//  keychaintest
//
//  Created by Won Beom Kim on 2016. 4. 20..
//  Copyright © 2016년 Won Beom Kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
